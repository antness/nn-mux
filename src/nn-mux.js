/**
 * Created by antness on 9/8/14.
 */
(function() {
    var m = angular.module('nnMux', []);
    m.provider('nnMux', ["$provide", function($provide) {
        var nnMuxProvider = {
            httpOptions: {},
            register: function(name, actions, config) {
                $provide.factory(name, ["$q", "$http", function($q, $http) {
                    var url = config.url;

                    var pendingCalls = [];
                    function throttle(callback, options) {
                        var timeout = null;
                        var previous = 0;
                        if (!options) options = {wait: 50};
                        var later = function(){
                            previous = 0;
                            timeout = null;
                            callback();
                        };
                        return function(){
                            var now = Date.now();
                            if(!previous)
                                previous = now;
                            var remaining = options.wait - (now - previous);
                            if(remaining <= 0 || remaining > options.wait) {
                                clearTimeout(timeout);
                                timeout = null;
                                previous = now;
                                callback();
                            } else if (!timeout) {
                                timeout = setTimeout(later, remaining);
                            }
                        };
                    }
                    function sendMuxRequest() {
                        if(pendingCalls.length === 0)
                            return;

                        var sendingCalls = pendingCalls.splice(0, pendingCalls.length);
                        var muxRequestData = [];
                        for(var i = 0; i < sendingCalls.length; ++i)
                            muxRequestData.push({action: sendingCalls[i].action, arguments: sendingCalls[i].arguments});

                        var requestOptions = angular.extend({}, nnMuxProvider.httpOptions, {
                            url: url,
                            method: 'POST',
                            headers: {'Content-Type': 'application/json'},
                            data: muxRequestData,
                            responseType: 'json'
                        });

                        $http(requestOptions).then(function(response){
                            var responses = response.data;
                            resolveResponses(responses);
                        }, function(error){
                            rejectResponses(error);
                        });

                        function resolveResponses(responses) {
                            for(var i = 0; i < responses.length; ++i)
                            {
                                var deffered = sendingCalls[i].deffered;
                                var response = responses[i];
                                if(response.success)
                                    deffered.resolve(response.data);
                                else
                                    deffered.reject(response);
                            }
                        }

                        function rejectResponses(error) {
                            for(var i = 0; i < sendingCalls.length; ++i)
                            {
                                var deffered = sendingCalls[i].deffered;
                                deffered.reject(error);
                            }
                        }
                    }
                    var throttledMuxRequest = throttle(sendMuxRequest, {wait: config.throttle});

                    function addPendingCall(action, args) {
                        var pendingCall = {
                            action: action,
                            arguments: args,
                            deffered: $q.defer()
                        };
                        pendingCalls.push(pendingCall);
                        throttledMuxRequest();
                        return pendingCall.deffered.promise;
                    }

                    function createCall(action) {
                        return function(args) {
                            return addPendingCall(action, args);
                        };
                    }

                    function convert(targetObject, prefix, actions) {
                        var isArray = angular.isArray(actions);
                        if(isArray)
                            angular.forEach(actions, function(value, key) {
                                targetObject[value] = createCall(prefix + value);
                            });
                        else
                            angular.forEach(actions, function(value, key) {
                                targetObject[key] = createCall(prefix + key);
                                if(angular.isObject(value) || angular.isArray(value))
                                    convert(targetObject[key], prefix + key + '.', value);
                            });
                    }

                    convert(addPendingCall, '', actions);

                    return addPendingCall;
                }]);
            },
            $get: function() {
                console.error('nnMux is a provider only');
            }
        };
        return nnMuxProvider;
    }]);
})();
